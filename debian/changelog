ibus-table-extraphrase (1.3.9.20110826-3) unstable; urgency=medium

  * debian/rules:
    + Use DEB_VERSION_UPSTREAM to provide version information.
      This should fix package reproducibility.
  * debian/patches: Add patch to let configure.ac to use external
    DEB_VERSION_UPSTREAM variable.
  * debian/control:
    + Bump Standards-Version to 4.4.1.
    + Bump debhelper compat to v12.

 -- Boyuan Yang <byang@debian.org>  Wed, 18 Dec 2019 13:33:05 -0500

ibus-table-extraphrase (1.3.9.20110826-2) unstable; urgency=medium

  * debian/control:
    + Update my email address and use the @debian.org one.
    + Bump Standards-Version to 4.2.1 (no changes needed).
    + Update homepage and use the project on GitHub.
    + Mark package ibus-table-extraphrase as Multi-Arch: same.

 -- Boyuan Yang <byang@debian.org>  Wed, 14 Nov 2018 21:23:12 -0500

ibus-table-extraphrase (1.3.9.20110826-1) unstable; urgency=medium

  * Adopt package and set maintainer to Debian Input Method Team.
    (Closes: #899550).
  * New upstream version 1.3.9.20110826.
    + Drop patches, merged upstream. (Closes: #768584)
  * Apply "wrap-and-sort -abst".
  * debian/control:
    + Bump debhelper compat to v11.
    + Drop autotools-dev build-dependency, no longer needed.
    + Bump Standards-Version to 4.1.5.
    + Use Google Code Archive as homepage.
    + Use git repo under Salsa input-method-team for Vcs fields.
    + Convert binary package into arch:any due to arch-specific contents.
  * debian/compat: Use compat v11.
  * debian/format: Use 3.0 (quilt) source package format.
  * debian/rules:
    + Convert to use dh sequencer.
    + Use "dh_missing --fail-missing".
  * debian/watch: Removed, upstream now defunct.

 -- Boyuan Yang <073plan@gmail.com>  Thu, 19 Jul 2018 19:20:49 +0800

ibus-table-extraphrase (1.2.0.20100305-2) unstable; urgency=medium

  * Team upload.
  * Rebuild binary data with ibus-table 1.9.1. Closes: #768149
  * Add binary dependency to ibus-table (>=1.9.1~).
  * Include this to IME Packaging Team.
  * Remove "頭發" from data/extra_phrase.txt to make this
    effectively the same as the latest upstream source 1.3.9.20110826
    fixing https://code.google.com/p/ibus/issues/detail?id=872 as:
    ...this is about the "extra phrases" table containing a
    non-existent word.  發 and 髮 both simplify to 发, but they are
    different characters where 發 only means 發達、發展 and 髮 only
    means 毛髮. hence only 頭髮 is correct.
  * Update Standards-Version 3.8.4 to 3.9.6 and watch file.
  * Add build-arch to debian/rules and kept debian/source/format.

 -- Osamu Aoki <osamu@debian.org>  Sat, 08 Nov 2014 17:33:17 +0900

ibus-table-extraphrase (1.2.0.20100305-1) unstable; urgency=low

  * New upstream release.
  * debian/control: bump standards version to 3.8.4.
  * debian/source/format: 1.0.

 -- LI Daobing <lidaobing@debian.org>  Wed, 24 Mar 2010 21:32:14 +0800

ibus-table-extraphrase (1.1.0.20090415-1) unstable; urgency=low

  * initial release (closes: #521632)

 -- LI Daobing <lidaobing@debian.org>  Thu, 23 Apr 2009 20:40:05 +0800
